﻿# Autonomy 
## Responsible: Ken Alberg, Rando Hinn
### Topic to be tackled: Platform mobility

### Description: Our mission is to move the platform from one reservoir to another. 
### We will use webcam to detect reservoirs from local area, sonar to keep right distance between platform and reservoir, turtlebot to move the unit and arduino nano to control the unit.

Required materials
 ### From Lab:
 
 - Turtlebot
 - Sonar
 - Arduino(s)
 - Webcam
 - POWER, if turtlebot does not have it integrated


 - RasPi for image processing (or we miiiight buy a tinkerboard for performance)


## Schedule:


By 11.11:

 - Autonomy & Waterworks roadmaps aligned, agreed on (3h)

By 18.11:

 - Put WafflePi together (4h)

By 25.11:

 - Get familiar with ROS,  install operation system on WafflePi and make it move (4h)

By 26.11:

 - Proof of concept

By 2.12:

 - Complete Integration with waterworks(4h)

 - The turtlebot is able to detect reservoirs using ArUco markers ( and move towards them) (8h)

By 12.12:

 - Project poster on PDF(co-op with Bucketology)

 - The turtlebot is able to move towards reservoirs, and keep the correct distance from them, inform Waterworks about the fact of reservoir presence, has a functional state machine-esque implementation, to know what it is doing. (8h)

 - Complete Integration with waterworks(4h)

By 17.12:

 - Demo, avoiding Murphy’s law
