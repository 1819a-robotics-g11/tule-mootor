set(_CATKIN_CURRENT_PACKAGE "tulemootor")
set(tulemootor_VERSION "0.0.0")
set(tulemootor_MAINTAINER "Rando Hinn <rando.hinn@tudeng.ut.ee>, Ken Alberg <ken.alberg@tudeng.ut.ee>")
set(tulemootor_PACKAGE_FORMAT "2")
set(tulemootor_BUILD_DEPENDS "rospy" "std_msgs")
set(tulemootor_BUILD_EXPORT_DEPENDS "rospy" "std_msgs")
set(tulemootor_BUILDTOOL_DEPENDS "catkin")
set(tulemootor_BUILDTOOL_EXPORT_DEPENDS )
set(tulemootor_EXEC_DEPENDS "rospy" "std_msgs")
set(tulemootor_RUN_DEPENDS "rospy" "std_msgs")
set(tulemootor_TEST_DEPENDS )
set(tulemootor_DOC_DEPENDS )
set(tulemootor_URL_WEBSITE "")
set(tulemootor_URL_BUGTRACKER "")
set(tulemootor_URL_REPOSITORY "")
set(tulemootor_DEPRECATED "")