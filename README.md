# Tule Mootor

## Building

For building, cd into the repo directory and run `catkin_make && catkin_make install`.
The install folder should be pushed to git.


## Config and setup

Since we do not have our project folder underneath the main catkin workspace, we need to add the path to the
built files to the `.bashrc`. For example as such.. `source ~/tule-mootor/devel/setup.bash --extend`. `--extend` is used so we don't overwrite the main
catkin devel files. On the turtlebot, replace devel with install. This could be mitigated by a move into
the main catkin workspace.

Also, be sure to `source ~/.bashrc`
